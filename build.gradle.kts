plugins {
    java
    id("net.researchgate.release") version "2.8.1"
}

release {
    pushReleaseVersionBranch = "releases"
}
